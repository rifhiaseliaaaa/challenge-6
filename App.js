import {StyleSheet, Text, View} from 'react-native';
import CrashlyticsScreen from './src/pages/CrashlyticsScreen';
import AnalyticsScreen from './src/pages/AnalyticsScreen';
import React, {useEffect} from 'react';
import messaging from '@react-native-firebase/messaging';
import MapScreen from './src/pages/MapScreen';
import QRCode from './src/pages/QRCode';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './src/pages/LoginScreen';
import WebScreen from './src/pages/WebScreen';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="MapScreen" component={MapScreen} />
      <Tab.Screen name="QRCode" component={QRCode} />
    </Tab.Navigator>
  );
};

export default function App() {
  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  const getToken = async () => {
    const token = await messaging().getToken();
    console.log(JSON.stringify(token));
  };

  useEffect(() => {
    requestUserPermission();
    getToken();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginScreen">
        <Stack.Screen
          name="MainApp"
          component={MainApp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="WebScreen"
          component={WebScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

