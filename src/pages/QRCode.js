import {CameraScreen, CameraType} from 'react-native-camera-kit';
// import {useIsFocused} from '@react-navigation/native';
import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const QRCode = ({navigation}) => {
//   const isFocused = useIsFocused();
  const onReadCode = data => {
    // alert(data.nativeEvent.codeStringValue);
    // ketika kamera dapat membaca qr code maka isi qr code akan dikrim ke hal webscreen
    navigation.navigate('WebScreen', {url: data.nativeEvent.codeStringValue})
  };

  return (
    <CameraScreen
      cameraType={CameraType.Back}
      scanBarcode={true}
      // onreadcode akan membaca barcode lalu isi barcode akan dikirim ke function onreadcode
      onReadCode={event => onReadCode(event)} // optional
      showFrame={true} // (default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
      laserColor="red" // (default red) optional, color of laser in scanner frame
      frameColor="white" // (default white) optional, color of border of scanner frame
    />
  ) ;
};

export default QRCode;

const styles = StyleSheet.create({});
