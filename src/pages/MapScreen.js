import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import React, {useEffect} from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

export default function MapScreen() {
  // untk menampilkan lokasi terkini, menambahkan geolocation pada useeffect
  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        alert(JSON.stringify(position));
      },
      error => {
        // See error code charts below.
        alert(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  });
  return (
    <View style={styles.container}>
      {/* // untuk menampilkan lokasi rumah, saya memiliki latitude dan longitude seperti dibawah*/}
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: -7.0034229,
          longitude: 107.6206558,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001,
        }}>
          {/* //ada 3 marker yang dibuat */}
        <Marker
          coordinate={{latitude: -7.0034229, longitude: 107.6206558}}
          title="My House"
          description="place where are sleep">
          <Callout>
            <View>
              <Text>My House</Text>
            </View>
          </Callout>
        </Marker>
        <Marker
          coordinate={{latitude: -7.008326, longitude: 107.6098743}}
          title="Pondok Pesantren"
          description="Pondok Pesantren Modern Al-Ihsan Baleendah">
          <Callout>
            <View>
              <Text>Pondok Pesantren</Text>
            </View>
          </Callout>
        </Marker>
        <Marker
          coordinate={{latitude: -7.0047164, longitude: 107.6193782}}
          title="Islamic School"
          description="Sekolah Alam Gaharu Baleendah">
          <Callout>
            <View>
              <Text>Islamic School</Text>
            </View>
          </Callout>
        </Marker>
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 1000,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
