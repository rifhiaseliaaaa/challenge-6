import {StyleSheet, Text, View, Button} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

const LoginScreen = ({navigation}) => {
  // Set an initializing state whilst Firebase connects
  // untuk menyimpan authentication  login sehingga ketika app di out login masih tersimpan
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    try {
      GoogleSignin.configure({
        webClientId:
          '850117420705-gjeu58qehnfvii64g0k21m3l26uapgt1.apps.googleusercontent.com',
      });
    } catch (error) {
      alert(error);
    }
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;
  // ketika function dipanggil pada on press maka dapar melakukan login dengan akun google
  async function onGoogleButtonPress() {
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  async function onGoogleSignOut() {
    return await auth().signOut();
  }
  // jika user tidak login maka akan menampilkan halaman login 
  if (!user) {
    return (
      <View>
        <Text>LoginScreen</Text>
        <GoogleSigninButton
          style={{width: 192, height: 48}}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          // jika login berhasil maka dapat menampilkan halaman MainApp
          onPress={() =>
            onGoogleButtonPress().then(() => navigation.replace('MainApp'))
          }
        />
      </View>
    );
  }
  // jika login berhasil maka dapat menampilkan halaman dibawah 
  return (
    <View>
      <Text>Welcome {user.email}</Text>
      <Button
        title="Sign-Out"
        onPress={() => onGoogleSignOut().then(() => console.log('Signed Out'))}
      />
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({});
